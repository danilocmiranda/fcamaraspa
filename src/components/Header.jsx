import React, { Component } from 'react';
import { AppBar } from 'material-ui';

const defaultStyle = {
  marginLeft: 20,
  flex: {
    flex: 1
  }
};

class Header extends Component {
  render() {
    return (
      <header className="header">
          <AppBar title="FCamara - Teste para FullStack Developer"/>
          <h1 style={defaultStyle} >Produtos</h1>
      </header>
    );
  }
}

export default Header;
