import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ProductItem from './ProductItem';
import { List } from 'material-ui';


const defaultStyle = {
  width: 300,
  marginLeft: 20
};

class MainSection extends Component {
  render() {
    const { products } = this.props;
    return (
      <section className="main" style={defaultStyle}>
        <List className="product-list">
          {products.map(product =>
            <ProductItem key={product.id} product={product} />
          )}
        </List>
      </section>
    );
  }
}

MainSection.propTypes = {
  products: PropTypes.array.isRequired,
};

export default MainSection;
