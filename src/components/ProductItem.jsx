import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { ListItem} from 'material-ui';
import Image from 'material-ui/svg-icons/image/image';

class ProductItem extends Component {

  render() {
    const { product } = this.props;
    return (
      <ListItem primaryText={product.name}
                secondaryText={product.description}
                leftIcon={<Image />}
      />
    );
  }
}

ProductItem.propTypes = {
  product: PropTypes.object.isRequired,
};

export default ProductItem;
