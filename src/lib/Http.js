import axios from 'axios'

const ROOT_URL = "http://localhost:8000/";

function getHeaders(){
  const token = localStorage.getItem('token')
  const headers = {
    'Accept': 'application/json',
    'Control-Allow-Origin': '*',
    'Content-Type': 'application/json'
  }

  if(token){
    console.log(token);
    headers['Authorization']= `Bearer ${token}`
  }

  return headers
}

export const get = (url, params) => {
  return axios({
    method: 'get',
    baseURL: `${ROOT_URL}`,
    url: url,
    params: params,
    headers: getHeaders()
  })
}

export const post = (url, params) => {
  return axios({
    method: 'post',
    baseURL: `${ROOT_URL}`,
    url: url,
    data: params,
    headers: getHeaders()
  })
}

export const put = (url, params) => {
  return axios({
    method: 'put',
    baseURL: `${ROOT_URL}`,
    url: url,
    data: params,
    headers: getHeaders()
  })
}
