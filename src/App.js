import React, { Component } from 'react';
import Header from './components/Header';
import MainSection from './components/MainSection';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import theme from './material_ui_raw_theme_file'
import { post, get } from './lib/Http'

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {products: []};
  }

  componentWillMount() {
    const products = [
    {"name": "Iphone4",
    "description": "apple smartphone"},
    {"name": "Iphone6",
    "description": "apple smartphone"},
    {"name": "Galaxy4",
    "description": "Android smartphone"},
    {"name": "Sony Ericson",
    "description": "Old Fashion"}
  ];
  products.forEach(function(product) {
      console.log("produto", product);
      post('/products/save', product)
      .then(function (response) {
        console.log("sucesso");
      })
      .catch((err) => {
        console.log("erro", err);
      })
      return product;
    });
  }

  componentDidMount() {
   this.loadProducts();
 }
  loadProducts() {
    get('/products').then(function (response) {
      this.setState({ products: response.data });
    })
    .catch(function(err) {
      console.log("erro", err);
    });
  }
  render() {
    return (
      <div>
        <MuiThemeProvider muiTheme={theme}>
          <div>
            <Header/>
            <MainSection products={this.state.products}/>
          </div>
        </MuiThemeProvider>
      </div>
    );
  }
}

export default App;
